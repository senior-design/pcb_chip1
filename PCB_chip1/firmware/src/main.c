/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes


// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

static void ACK2_User_Handler(GPIO_PIN pin, uintptr_t context)
{
    
    ACK1_Clear();
}

void sendMessage(uint8_t message){
    while(ACK2_Get()!= 0){
    }
    //least significant bit
    if (message & 0x1){
        F11_Set();
    }
    else{
        F11_Clear();
    }
    
    if ((message>>1) & 0x1){
        F12_Set();
    }
    else{
        F12_Clear();
    }
    
    if ((message>>2) & 0x1){
        F13_Set();
    }
    else{
        F13_Clear();
    }
    
    if ((message>>3) & 0x1){
        F14_Set();
    }
    else{
        F14_Clear();
    }
    
    if ((message>>4) & 0x1){
        F15_Set();
    }
    else{
        F15_Clear();
    }
    
    if ((message>>5) & 0x1){
        F16_Set();
    }
    else{
        F16_Clear();
    }
    
    if ((message>>6) & 0x1){
        F41_Set();
    }
    else{
        F41_Clear();
    }
    
    if ((message>>7) & 0x1){
        F42_Set();
    }
    else{
        F42_Clear();
    }
    
    ACK1_Set();
}

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    GPIO_PinInterruptCallbackRegister(ACK2_PIN , ACK2_User_Handler, 0);
    GPIO_PinInterruptEnable(ACK2_PIN);
    
    ACK1_Clear();
    uint8_t test_array[] = {1, 2, 3};
    while ( true )
    {
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
        int i = 0;
        CORETIMER_DelayMs(2000);
        sendMessage(test_array[i]);
        i =  (i + 1)%3;
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

