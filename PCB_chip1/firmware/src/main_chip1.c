/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include <math.h>

#define ADC_VREF                (3.3f)
#define ADC_MAX_COUNT           (4095)  //12 bit resolution
#define NUM_RATIOS 81

#define POINTER_F 1.25
#define MIDDLE_F 1.5
#define RING_F 1.65

#define VIBRATO_0 1.5
#define VIBRATO_INC 0.2

//int ring_f[9] = {50, 52, 54, 60, 64, 70, 74, 76, 78};
//int pointer_f[9] = {2, 8, 12, 14, 16, 22, 26, 32, 36};
//int middle_f[9] = {24, 26, 32, 36, 42, 46, 48, 50, 56};

int ring_f[8] = {50, 56, 60, 62, 64, 70, 74, 80}; //4, 5, 6
int pointer_f[6] = {2, 8, 12, 14, 17, 22}; 
int middle_f[6] = {26, 32, 36, 38, 40, 46};  



//Ring Finger = F1
//Pointer Finger = F2
//Middle Finger = F3
//ADC Notes:
//ADC17 -> NHBZ
//ADC2 -> NHF1Z
//ADC5 -> NHF2Z
//ADC8 -> NHF3Z
//ADC13 -> EHBZ
//ADC11 -> EHBX
//ADC12 -> EHBY
    //down = 1.75
    //middle =  1.6
    //up = 1.35
// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************
static void ACK2_User_Handler(GPIO_PIN pin, uintptr_t context)
{
//    bool temp = false;
//    temp = GPIO_PinRead(ACK2_PIN);
//    if(temp){
//        CORETIMER_DelayMs(10);
//        ACK1_Clear();
//    }
    ACK1_Clear();
}

void sendMessage(uint8_t message){
    while(GPIO_PinRead(ACK2_PIN)|GPIO_PinRead(ACK1_PIN)){
        CORETIMER_DelayMs(10);
    }
    //CORETIMER_DelayMs(1000);
    //CORETIMER_DelayMs(1000);
    /*if (temp == 1){
        F11_Set();
        F12_Clear();
    }
    else if (message == 2){
        F11_Clear();
        F12_Set();
    }
    else if (message == 3){
        F11_Set();
        F12_Set();
    }*/
    //least significant bit
    
    if (message & 0x1){
        F11_Set();
    }
    else{
        F11_Clear();
    }
    
    if ((message>>1) & 0x1){
        F12_Set();
    }
    else{
        F12_Clear();
    }
    
    if ((message>>2) & 0x1){
        F13_Set();
    }
    else{
        F13_Clear();
    }
    
    if ((message>>3) & 0x1){
        F14_Set();
    }
    else{
        F14_Clear();
    }
    
    if ((message>>4) & 0x1){
        F15_Set();
    }
    else{
        F15_Clear();
    }
    
    if ((message>>5) & 0x1){
        F16_Set();
    }
    else{
        F16_Clear();
    }
    
    if ((message>>6) & 0x1){
        F41_Set();
    }
    else{
        F41_Clear();
    }
    
    if ((message>>7) & 0x1){
        F42_Set();
    }
    else{
        F42_Clear();
    }
    
    ACK1_Set();
}

float readADC(ADCHS_CHANNEL_NUM channel){
    /* Wait till ADC conversion result is available */
    while(!ADCHS_ChannelResultIsReady(channel))
    {

    };
    uint16_t adc_count = ADCHS_ChannelResultGet(channel);
    float adc_voltage = (float)adc_count * ADC_VREF / ADC_MAX_COUNT;
    
    return adc_voltage;
}
int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    ACK1_Clear();
    F11_Clear();
    F12_Clear();
    
    F13_Clear();
    
    F14_Clear();
    F15_Clear();
    F16_Clear();
    F41_Clear();
    F42_Clear();
    
    GPIO_PinInterruptCallbackRegister(ACK2_PIN , ACK2_User_Handler, 0);
    GPIO_PinInterruptEnable(ACK2_PIN);
    
    CORETIMER_DelayMs(3000);
    
    //uint8_t test_array[] = {1, 2, 3};
    //int i = 0;
    int vibrato_ind = 0;
    int vibrato_dir = 1;
    uint8_t message_pointer = 0;
    uint8_t message_middle = 0;
    uint8_t message_ring = 0;
    float margin = 0.03;
    
    while ( true )
    {
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks();
        //uint8_t nhbz = (uint8_t)((readADC(ADCHS_CH17) - 1.65)*NUM_RATIOS/0.34) - 1;
        
        ADCHS_GlobalEdgeConversionStart();
        float nhbz = readADC(ADCHS_CH17);
        float nhf1z = readADC(ADCHS_CH2);
        float nhf2z = readADC(ADCHS_CH5);
        float nhf3z = readADC(ADCHS_CH8);
        //float ehbz = readADC(ADCHS_CH13);
        float ehby = readADC(ADCHS_CH12);
        float ehbx = readADC(ADCHS_CH11);
        ADCHS_GlobalLevelConversionStop();
        
        //uint8_t message = (uint8_t)((nhbz - nhf2z)*NUM_RATIOS/0.34)-1;
        //message = message - message % 8; //hit every 8th note
        
        //Note generation
        //Experimental z voltage range is approximately 1.5V to 1.9V
        
        float lb_p = (message_pointer/14.0);
        float ub_p = ((message_pointer+1)/14.0);
        
        float lb_m = (message_middle/14.0);
        float ub_m = ((message_middle+1)/14.0);
        
        float lb_r = (message_ring/18.0);
        float ub_r = ((message_ring+1)/18.0);
        
        if ( fabs(nhbz - nhf2z + 0.05) < (lb_p - margin) || fabs(nhbz - nhf2z + 0.05) > (ub_p + margin) ){
            message_pointer = (uint8_t)(abs((int)((nhbz - nhf2z + 0.05)*14)));
        }
        if( fabs(nhbz - nhf3z + 0.05) < (lb_m - margin) || fabs(nhbz - nhf3z + 0.05) > (ub_m + margin) ){
            message_middle = (uint8_t)(abs((int)((nhbz - nhf3z + 0.05)*14)));
        }
        if( fabs(nhbz - nhf1z) < (lb_r - margin) || fabs(nhbz - nhf1z) > (ub_r + margin) ){
            message_ring = (uint8_t)(abs((int)((nhbz - nhf1z)*18)));
        }
        
        if (message_pointer > 5){
            message_pointer = 5;
        }
        if (message_middle > 5){
            message_middle = 5;
        }
        if (message_ring > 7){
            message_ring = 7;
        }
        //Vibrato
        int thresh_v = abs((int)((ehbx-VIBRATO_0)/VIBRATO_INC));
        if (thresh_v == 0){
            vibrato_ind = 0;
        }
        else{
            if (vibrato_ind >= thresh_v || vibrato_ind <= -thresh_v){
                vibrato_dir = -vibrato_dir;
            }
            vibrato_ind = vibrato_ind + vibrato_dir;
        }
        
        //pointer
        if (ehby < MIDDLE_F){
            uint8_t tx = pointer_f[message_pointer] + vibrato_ind;
            if(tx < 0){
                tx = 0;
            }
            if (tx >= NUM_RATIOS){
                tx = NUM_RATIOS - 1;
            }
            sendMessage(tx);
        }
        //middle
        else if (ehby > MIDDLE_F && ehby < RING_F ){
            uint8_t tx = middle_f[message_middle] + vibrato_ind;
            if(tx < 0){
                tx = 0;
            }
            if (tx >= NUM_RATIOS){
                tx = NUM_RATIOS - 1;
            }
            sendMessage(tx);
        }
        //ring
        else if (ehby > RING_F){
            uint8_t tx = ring_f[message_ring] + vibrato_ind;
            if(tx < 0){
                tx = 0;
            }
            if (tx >= NUM_RATIOS){
                tx = NUM_RATIOS - 1;
            }
            sendMessage(tx);
        }
        //else{
        //    sendMessage(100);
        //}
//        if (message > 8){
//            message = 8; 
//        }
        
        //if(ehbz<1.85){
        //sendMessage(middle_f[message]);
        //}
        //else{
        //    sendMessage(message);
        //}
        //i =  (i + 1)%3;
        CORETIMER_DelayMs(75);
    }
    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

